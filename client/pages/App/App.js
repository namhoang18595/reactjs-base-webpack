import React from 'react';
import './App.scss';
import Routes from '../../rootRouter';

const App = () => (
  <div className="App">
    <Routes />
  </div>
);

export default App;
