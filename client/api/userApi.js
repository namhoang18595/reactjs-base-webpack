import baseApi from './baseApi';

export const loginApi = (user) => {
  const { username, password } = user;
  return baseApi.post('/login', {
    username,
    password
  });
};

export const signupApi = user => baseApi.post('/signup', { user });
