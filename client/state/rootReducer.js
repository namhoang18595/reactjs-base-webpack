
import { combineReducers } from 'redux'

import { reducer as location } from './modules/routing'
import { reducer as formReducer } from 'redux-form'
import testReducer from './modules/testing' //test
import auth from './modules/auth/index'

export const reducers = combineReducers({
  location, 
  form: formReducer,
  auth,
  test: testReducer
})
