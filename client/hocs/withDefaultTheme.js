import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2196F3'
    }
  },
});

export const withDefaultTheme = WrappedComponent => (props) => { // eslint-disable-line
  return (
    <MuiThemeProvider theme={theme}>
      <WrappedComponent {...props} />
    </MuiThemeProvider>
  );
};
