import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

const styles = {
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
};

const SideList = ({ classes }) => (
  <div className={classes.list}>
    <List>
      {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
        <ListItem button key={text}>
          <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
          <ListItemText primary={text} />
        </ListItem>
      ))}
    </List>
    <Divider />
    <List>
      {['All mail', 'Trash', 'Spam'].map((text, index) => (
        <ListItem button key={text}>
          <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
          <ListItemText primary={text} />
        </ListItem>
      ))}
    </List>
  </div>
);

SideList.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired
};

const TemporaryDrawer = ({ openDrawer, onClose, classes }) => (
  <div>
    <Drawer open={openDrawer} onClose={onClose}>
      <div
        tabIndex={0}
        role="button"
        onClick={onClose}
        onKeyDown={onClose}
      >
        <SideList classes={classes} />
      </div>
    </Drawer>
  </div>
);

TemporaryDrawer.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  onClose: PropTypes.func.isRequired,
  openDrawer: PropTypes.func.isRequired

};

export default withStyles(styles)(TemporaryDrawer);
