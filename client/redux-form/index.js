import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';

export const renderTextField = ({ //eslint-disable-line
  input,
  label,
  type
}) => (
  <div>
    <TextField
      style={{
        width: '100%'
      }}
      label={label}
      type={type}
      {...input}
    />
  </div>
);

renderTextField.propTypes = {
  input: PropTypes.instanceOf(Object).isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};
